using System;
using System.Linq;
using Godot;
using Godot.Collections;

/// <summary>
/// DockPanel is used to size and position children inward from the edges of available space.
///
/// A <see cref="Dock" /> enum (see <see cref="SetDock" /> and <see cref="GetDock" />)
/// determines on which size a child is placed.  Children are stacked in order from these edges until
/// there is no more space; this happens when previous children have consumed all available space, or a child
/// with Dock set to Fill is encountered.
/// </summary>
[GlobalClass]
[Tool]
public partial class DockPanel : Container
{
    [Export]
    private Dictionary<StringName, Dock> docks = new();

    private bool lastChildFill;
    //-------------------------------------------------------------------
    //
    //  Public Methods
    //
    //-------------------------------------------------------------------

    #region Public Methods

    /// <summary>
    /// Reads the attached property Dock from the given element.
    /// </summary>
    /// <param name="element">UIElement from which to read the attached property.</param>
    /// <returns>The property's value.</returns>
    /// <seealso cref="DockPanel.DockProperty" />
    public Dock GetDock(Control element)
    {
        return GetDock(element.Name);
    }

    public Dock GetDock(StringName element)
    {
        if (!docks.ContainsKey(element))
            docks.Add(element, Dock.Center);
        return docks[element];
    }

    public void SetDock(StringName elementName, Dock dock)
    {
        docks[elementName] = dock;
    }

    public void SetDock(Control element, Dock dock)
    {
        SetDock(element.Name, dock);
    }

    public override Vector2 _GetMinimumSize()
    {
        return ArrangeOverride(base._GetMinimumSize());
    }

    public override void _Notification(int what)
    {
        switch ((long)what)
        {
            case NotificationSortChildren:
                MeasureOverride(Size);
                break;

            case NotificationThemeChanged:
                UpdateMinimumSize();
                break;

            case NotificationTranslationChanged:
            case NotificationLayoutDirectionChanged:
                QueueSort();
                break;
        }
    }

    #endregion

    public override Array<Dictionary> _GetPropertyList()
    {
        Array<Dictionary> properties = new();
        foreach (Node child in GetChildren())
        {
            properties.Add(new Dictionary
            {
                { "name", $"{nameof(Dock)}/{child.Name}" },
                { "type", (int)Variant.Type.Int },
                { "hint", (int)PropertyHint.Enum },
                { "hint_string", string.Join(",", Enum.GetNames(typeof(Dock))) }
            });
        }

        return properties;
    }

    public override Variant _Get(StringName property)
    {
        if ((property + "").Contains($"{nameof(Dock)}/"))
        {
            string name = property.ToString()?.Replace($"{nameof(Dock)}/", "");
            return (int)GetDock(name);
        }

        return base._Get(property);
    }

    public override bool _Set(StringName property, Variant value)
    {
        if ((property + "").Contains($"{nameof(Dock)}/"))
        {
            string name = property.ToString()?.Replace($"{nameof(Dock)}/", "");
            SetDock(name, (Dock)Enum.ToObject(typeof(Dock), value.AsInt32()));
            _Resort();
        }

        return base._Set(property, value);
    }


    //-------------------------------------------------------------------
    //
    //  Protected Methods
    //
    //-------------------------------------------------------------------

    #region Protected Methods

    protected void _Resort()
    {
        ArrangeOverride(Size);
        UpdateMinimumSize();
    }

    /// <summary>
    /// Updates DesiredSize of the DockPanel.  Called by parent UIElement.  This is the first pass of layout.
    /// </summary>
    /// <remarks>
    /// Children are measured based on their sizing properties and <see cref="Dock" />.  
    /// Each child is allowed to consume all of the space on the side on which it is docked; Left/Right docked
    /// children are granted all vertical space for their entire width, and Top/Bottom docked children are
    /// granted all horizontal space for their entire height.
    /// </remarks>
    /// <param name="constraint">Constraint size is an "upper limit" that the return value should not exceed.</param>
    /// <returns>The Panel's desired size.</returns>
    protected Vector2 MeasureOverride(Vector2 constraint)
    {
        GD.Print("MeasureOverride");
        float parentWidth = 0; // Our current required width due to children thus far.
        float parentHeight = 0; // Our current required height due to children thus far.
        float accumulatedWidth = 0; // Total width consumed by children.
        float accumulatedHeight = 0; // Total height consumed by children.

        foreach (Node node in GetChildren())
        {
            Vector2 childConstraint; // Contains the suggested input constraint for this child.
            Vector2 childDesiredSize; // Contains the return size from child measure.

            if (node is not Control child)
                continue;

            // Child constraint is the remaining size; this is total size minus size consumed by previous children.
            childConstraint = new Vector2(
                MathF.Max(0.0F, constraint.X - accumulatedWidth),
                MathF.Max(0.0F, constraint.Y - accumulatedHeight)
            );

            // Measure child.
            // child.CustomMinimumSize = childConstraint;
            child.CustomMinimumSize = Measure(node, childConstraint);
            GD.Print($"\t{node.Name} ({GetDock(child)}): {child.CustomMinimumSize}");
            childDesiredSize = child.GetCombinedMinimumSize();

            // Now, we adjust:
            // 1. Size consumed by children (accumulatedSize).  This will be used when computing subsequent
            //    children to determine how much space is remaining for them.
            // 2. Parent size implied by this child (parentSize) when added to the current children (accumulatedSize).
            //    This is different from the size above in one respect: A Dock.Left child implies a height, but does
            //    not actually consume any height for subsequent children.
            // If we accumulate size in a given dimension, the next child (or the end conditions after the child loop)
            // will deal with computing our minimum size (parentSize) due to that accumulation.
            // Therefore, we only need to compute our minimum size (parentSize) in dimensions that this child does
            //   not accumulate: Width for Top/Bottom, Height for Left/Right.
            switch (GetDock(child))
            {
                case Dock.Left:
                case Dock.Right:
                    parentHeight = Math.Max(parentHeight, accumulatedHeight + childDesiredSize.Y);
                    accumulatedWidth += childDesiredSize.X;
                    break;

                case Dock.Top:
                case Dock.Bottom:
                    parentWidth = Math.Max(parentWidth, accumulatedWidth + childDesiredSize.X);
                    accumulatedHeight += childDesiredSize.Y;
                    break;
            }
        }

        // Make sure the final accumulated size is reflected in parentSize.
        parentWidth = MathF.Max(parentWidth, accumulatedWidth);
        parentHeight = MathF.Max(parentHeight, accumulatedHeight);

        GD.Print("\tself: " + new Vector2(parentWidth, parentHeight));

        return (new Vector2(parentWidth, parentHeight));
    }

    private Vector2 Measure(Node node, Vector2 childConstraint)
    {
        Vector2 size = Vector2.Zero;
        if (node.GetChildCount() > 0)
            size = node.GetChildren().Max(child => Measure(child, childConstraint));
        else
        {
            if (node is Control control)
                size = control.Position + control.GetMinimumSize();
        }

        return size;
        // return new Vector2(Mathf.Max(size.X, childConstraint.X), Mathf.Max(size.Y, childConstraint.Y));
    }

    /// <summary>
    /// DockPanel computes a position and final size for each of its children based upon their
    /// <see cref="Dock" /> enum and sizing properties.
    /// </summary>
    /// <param name="arrangeSize">Size that DockPanel will assume to position children.</param>
    protected Vector2 ArrangeOverride(Vector2 arrangeSize)
    {
        GD.Print("ArrangeOverride");
        Array<Node> children = GetChildren();

        float accumulatedLeft = 0;
        float accumulatedTop = 0;
        float accumulatedRight = 0;
        float accumulatedBottom = 0;


        foreach (Node node in children)
        {
            if (node is not Control child)
                continue;

            Vector2 childDesiredSize = child.GetCombinedMinimumSize();
            var rcChildPos = new Vector2(accumulatedLeft, accumulatedTop);
            var rcChildSize = new Vector2(
                MathF.Max(0.0F, arrangeSize.X - (accumulatedLeft + accumulatedRight)),
                MathF.Max(0.0F, arrangeSize.Y - (accumulatedTop + accumulatedBottom))
            );

            switch (GetDock(child))
            {
                case Dock.Left:
                    accumulatedLeft += childDesiredSize.X;
                    rcChildSize.X = childDesiredSize.X;
                    break;

                case Dock.Right:
                    accumulatedRight += childDesiredSize.X;
                    rcChildPos.X = MathF.Max(0.0F, arrangeSize.X - accumulatedRight);
                    rcChildSize.X = childDesiredSize.X;
                    break;

                case Dock.Top:
                    accumulatedTop += childDesiredSize.Y;
                    rcChildSize.Y = childDesiredSize.Y;
                    break;

                case Dock.Bottom:
                    accumulatedBottom += childDesiredSize.Y;
                    rcChildPos.Y = MathF.Max(0.0F, arrangeSize.Y - accumulatedBottom);
                    rcChildSize.Y = childDesiredSize.Y;
                    break;
            }


            child.SetSize(rcChildSize);
            child.SetPosition(rcChildPos);
        }

        GD.Print("\t" + arrangeSize);
        return (arrangeSize);
    }

    #endregion Protected Methods
}

#region Dock enum type

/// <summary>
/// Dock - Enum which describes how to position and stretch the child of a DockPanel.
/// </summary>
/// <seealso cref="DockPanel" />
public enum Dock
{
    /// <summary>
    /// Position this child at the left of the remaining space.
    /// </summary>
    Center = 0,

    /// <summary>
    /// Position this child at the left of the remaining space.
    /// </summary>
    Left = 1,

    /// <summary>
    /// Position this child at the top of the remaining space.
    /// </summary>
    Top = 2,

    /// <summary>
    /// Position this child at the right of the remaining space.
    /// </summary>
    Right = 3,

    /// <summary>
    /// Position this child at the bottom of the remaining space.
    /// </summary>
    Bottom = 4
}

#endregion