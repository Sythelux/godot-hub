// using System.Collections.Generic;
// using System.IO;
// using System.Linq;
// using System.Threading.Tasks;
// using Godot;
// using Godot.Collections;
// using Directory = System.IO.Directory;
// using File = System.IO.File;
//
// public partial class Main : Node
// {
//     public override void _Ready()
//     {
//         Globals.SceneRef = GetTree();
//
//
//         //Hook static ui
//         Globals.SceneRef.Root.GetNodeOrNull<Button>("/root/GodotHub/WindowDialogChangeLog/VBoxContainer/HBoxContainer2/Button").Connect("pressed", GetNode("."), nameof(HideChangelog));
//         Globals.SceneRef.Root.GetNodeOrNull<Button>("/root/GodotHub/Main/VBoxContainer/TopBar/HBoxContainer2/Button").Connect("pressed", GetNode("."), nameof(Setup));
//
//         if (!Directory.Exists(Globals.DataFolderPath))
//         {
//             Directory.CreateDirectory(Globals.DataFolderPath);
//         }
//
//         if (!Directory.Exists($"{Globals.DataFolderPath}Instances"))
//         {
//             Directory.CreateDirectory($"{Globals.DataFolderPath}Instances");
//         }
//
//         //Create and list existing instances
//
//         var installedInstanceRow = GD.Load<PackedScene>("res://InstalledInstanceRow.tscn");
//         foreach (var item in Directory.GetDirectories($"{Globals.DataFolderPath}Instances{Path.DirectorySeparatorChar}"))
//         {
//             if (installedInstanceRow.Instantiate<HBoxContainer>() is { } instancedInstalledInstanceRow)
//             {
//                 var dir = new DirectoryInfo(item);
//                 instancedInstalledInstanceRow.Name = dir.Name;
//                 Globals.SceneRef.Root.GetNodeOrNull<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Installed/VBoxContainer").AddChild(instancedInstalledInstanceRow);
//                 Globals.SceneRef.Root.GetNodeOrNull<Label>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Installed/VBoxContainer/{instancedInstalledInstanceRow.Name}/VersionNumber").Text = instancedInstalledInstanceRow.Name;
//             }
//         }
//
//         Task.Run(Setup);
//     }
//
//     public static void ShowChangelog(string url, Definitions.ReleaseTypes type, string version, string subVersion)
//     {
//
//         // Globals.SceneRef.Root.GetNodeOrNull<WindowDialog>("/root/GodotHub/WindowDialogChangeLog").Show();
//         Globals.SceneRef.Root.GetNodeOrNull<Label>("/root/GodotHub/WindowDialogChangeLog/VBoxContainer/Title").Text = $" Godot {version} {subVersion} {type}";
//         Globals.SceneRef.Root.GetNodeOrNull<RichTextLabel>("/root/GodotHub/WindowDialogChangeLog/VBoxContainer/RichTextLabel").Text = Utilities.GetStringFromWebRequest(url);
//     }
//
//     public static void HideChangelog()
//     {
//         // Globals.SceneRef.Root.GetNodeOrNull<WindowDialog>("/root/GodotHub/WindowDialogChangeLog").Hide();
//     }
//
//     public static async Task Setup()
//     {
//         Globals.RCReleases = new List<Definitions.Release>();
//         Globals.BetaReleases = new List<Definitions.Release>();
//         Globals.AlphaReleases = new List<Definitions.Release>();
//         // Globals.StableReleases.Reverse();
//         Globals.RCReleases.Reverse();
//         Globals.BetaReleases.Reverse();
//         Globals.AlphaReleases.Reverse();
//
//
//
//
//         //Set Versions
//         Globals.SceneRef.Root.GetNode<Label>("/root/GodotHub/Main/VBoxContainer/TopBar/HBoxContainer2/Label2").Text = Globals.StableReleases.FirstOrDefault()?.Version;
//         Globals.SceneRef.Root.GetNode<Label>("/root/GodotHub/Main/VBoxContainer/TopBar/HBoxContainer2/Label8").Text = $"{Globals.RCReleases.FirstOrDefault()?.Version} - {Globals.RCReleases.FirstOrDefault()?.SubVersion}";
//         Globals.SceneRef.Root.GetNode<Label>("/root/GodotHub/Main/VBoxContainer/TopBar/HBoxContainer2/Label4").Text = $"{Globals.BetaReleases.FirstOrDefault()?.Version} - {Globals.BetaReleases.FirstOrDefault()?.SubVersion}";
//         Globals.SceneRef.Root.GetNode<Label>("/root/GodotHub/Main/VBoxContainer/TopBar/HBoxContainer2/Label6").Text = $"{Globals.AlphaReleases.FirstOrDefault()?.Version} - {Globals.AlphaReleases.FirstOrDefault()?.SubVersion}";
//         //Build menus
//
//
//         var installInstanceRow = GD.Load<PackedScene>("res://InstallInstanceRow.tscn");
//
//
//         foreach (Node item in Globals.SceneRef.Root.GetNode<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Stable/VBoxContainer").GetChildren())
//         {
//             item.QueueFree();
//         }
//
//
//         foreach (var item in Globals.StableReleases)
//         {
//             if (installInstanceRow.Instantiate<HBoxContainer>() is { } instancedInstallInstanceRow)
//             {
//                 instancedInstallInstanceRow.Name = item.Version + item.SubVersion;
//                 Globals.SceneRef.Root.GetNodeOrNull<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Stable/VBoxContainer/").AddChild(instancedInstallInstanceRow);
//                 Globals.SceneRef.Root.GetNodeOrNull<Label>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Stable/VBoxContainer/{instancedInstallInstanceRow.Name}/Label2").Text = item.Version + item.SubVersion;
//
//                 if (item.Changelog == "")
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Stable/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").QueueFree();
//                 }
//                 else
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Stable/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(ShowChangelog), new Array { item.Changelog, item.ReleaseType, item.Version, item.SubVersion });
//                 }
//
//                 Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Stable/VBoxContainer/{instancedInstallInstanceRow.Name}/Button2").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstanceSetUi), new Array { item.export_templates, item.Mono_export_templates, item.Source, item.win64, item.Mono_win64, item.x11, item.Mono_x11, item.headless, item.server, item.Mono_headless, item.Mono_server, item.Version, item.SubVersion, item.ReleaseType.ToString() });
//             }
//         }
//
//         foreach (Node item in Globals.SceneRef.Root.GetNode<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Release Candidate/VBoxContainer").GetChildren())
//         {
//             item.QueueFree();
//         }
//
//         foreach (var item in Globals.RCReleases)
//         {
//             if (installInstanceRow.Instance() is HBoxContainer instancedInstallInstanceRow)
//             {
//                 instancedInstallInstanceRow.Name = item.Version + item.SubVersion;
//                 Globals.SceneRef.Root.GetNodeOrNull<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Release Candidate/VBoxContainer/").AddChild(instancedInstallInstanceRow);
//                 Globals.SceneRef.Root.GetNodeOrNull<Label>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Release Candidate/VBoxContainer/{instancedInstallInstanceRow.Name}/Label2").Text = item.Version + item.SubVersion;
//
//                 if (item.Changelog == "")
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Release Candidate/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").QueueFree();
//                 }
//                 else
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Release Candidate/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub/"), nameof(ShowChangelog), new Array { item.Changelog, item.ReleaseType, item.Version, item.SubVersion });
//                 }
//
//                 Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Release Candidate/VBoxContainer/{instancedInstallInstanceRow.Name}/Button2").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstanceSetUi), new Array { item.export_templates, item.Mono_export_templates, item.Source, item.win64, item.Mono_win64, item.x11, item.Mono_x11, item.headless, item.server, item.Mono_headless, item.Mono_server, item.Version, item.SubVersion, item.ReleaseType.ToString() });
//             }
//         }
//
//         foreach (Node item in Globals.SceneRef.Root.GetNode<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Beta/VBoxContainer").GetChildren())
//         {
//             item.QueueFree();
//         }
//
//         foreach (var item in Globals.BetaReleases)
//         {
//             if (installInstanceRow.Instance() is HBoxContainer instancedInstallInstanceRow)
//             {
//                 instancedInstallInstanceRow.Name = item.Version + item.SubVersion;
//                 Globals.SceneRef.Root.GetNodeOrNull<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Beta/VBoxContainer/").AddChild(instancedInstallInstanceRow);
//                 Globals.SceneRef.Root.GetNodeOrNull<Label>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Beta/VBoxContainer/{instancedInstallInstanceRow.Name}/Label2").Text = item.Version + item.SubVersion;
//
//                 if (item.Changelog == "")
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Beta/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").QueueFree();
//                 }
//                 else
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Beta/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub/"), nameof(ShowChangelog), new Array { item.Changelog, item.ReleaseType, item.Version, item.SubVersion });
//                 }
//
//                 Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Beta/VBoxContainer/{instancedInstallInstanceRow.Name}/Button2").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstanceSetUi), new Array { item.export_templates, item.Mono_export_templates, item.Source, item.win64, item.Mono_win64, item.x11, item.Mono_x11, item.headless, item.server, item.Mono_headless, item.Mono_server, item.Version, item.SubVersion, item.ReleaseType.ToString() });
//             }
//         }
//
//         foreach (Node item in Globals.SceneRef.Root.GetNode<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Alpha/VBoxContainer").GetChildren())
//         {
//             item.QueueFree();
//         }
//
//
//
//         foreach (var item in Globals.AlphaReleases)
//         {
//             if (installInstanceRow.Instance() is HBoxContainer instancedInstallInstanceRow)
//             {
//                 instancedInstallInstanceRow.Name = item.Version + item.SubVersion;
//                 Globals.SceneRef.Root.GetNodeOrNull<VBoxContainer>("/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Alpha/VBoxContainer/").AddChild(instancedInstallInstanceRow);
//                 Globals.SceneRef.Root.GetNodeOrNull<Label>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Alpha/VBoxContainer/{instancedInstallInstanceRow.Name}/Label2").Text = item.Version + item.SubVersion;
//
//                 if (item.Changelog == "")
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Alpha/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").QueueFree();
//                 }
//                 else
//                 {
//                     Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Alpha/VBoxContainer/{instancedInstallInstanceRow.Name}/Button6").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub/"), nameof(ShowChangelog), new Array { item.Changelog, item.ReleaseType, item.Version, item.SubVersion });
//                 }
//
//                 Globals.SceneRef.Root.GetNodeOrNull<Button>($"/root/GodotHub/Main/VBoxContainer/TabContainer/Install/Alpha/VBoxContainer/{instancedInstallInstanceRow.Name}/Button2").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstanceSetUi), new Array { item.export_templates, item.Mono_export_templates, item.Source, item.win64, item.Mono_win64, item.x11, item.Mono_x11, item.headless, item.server, item.Mono_headless, item.Mono_server, item.Version, item.SubVersion, item.ReleaseType.ToString() });
//             }
//         }
//     }
//
//     public static void InstallInstanceSetUi(string exportTemplates, string monoExportTemplates, string source, string win64, string monoWin64, string x11, string monoX11, string headless, string server, string monoHeadless, string monoServer, string version, string subVersion, string releaseType)
//     {
//         //Default ui
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButtonExportTemplates").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButton2ExportTemplatesMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButtonSource").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerWindows/CheckButtonEditor").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerWindows/CheckButtonEditorMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinux/CheckButtonEditor").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinux/CheckButtonEditorMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonHeadless").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonServer").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonHeadlessMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonServerMono").Pressed = false;
//
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerExportsSource/CheckButtonExportTemplates").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerExportsSource/CheckButton2ExportTemplatesMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerWindows/CheckButtonEditor").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerWindows/CheckButtonEditorMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinux/CheckButtonEditor").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinux/CheckButtonEditorMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonHeadless").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonServer").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonHeadlessMono").Pressed = false;
//         Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonServerMono").Pressed = false;
//
//
//
//         if (exportTemplates == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButtonExportTemplates").Disabled = true;
//         }
//         if (monoExportTemplates == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButton2ExportTemplatesMono").Disabled = true;
//         }
//         if (source == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButtonSource").Disabled = true;
//         }
//         if (win64 == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerWindows/CheckButtonEditor").Disabled = true;
//         }
//         if (monoWin64 == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerWindows/CheckButtonEditorMono").Disabled = true;
//         }
//         if (x11 == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinux/CheckButtonEditor").Disabled = true;
//         }
//         if (monoX11 == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinux/CheckButtonEditorMono").Disabled = true;
//         }
//         if (headless == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonHeadless").Disabled = true;
//         }
//         if (server == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonServer").Disabled = true;
//         }
//         if (monoHeadless == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonHeadlessMono").Disabled = true;
//         }
//         if (monoServer == "")
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonServerMono").Disabled = true;
//         }
//
//
//         //TODO UNLINK OLDER LINKS; OR DELETE AND RECREATE BUTTON
//
//         if (Globals.SceneRef.Root.GetNodeOrNull<Button>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/HBoxContainerAcceptCancel/Accept").IsConnected("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstance)))
//         {
//             Globals.SceneRef.Root.GetNodeOrNull<Button>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/HBoxContainerAcceptCancel/Accept").Disconnect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstance));
//         }
//         
//         Globals.SceneRef.Root.GetNodeOrNull<Button>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/HBoxContainerAcceptCancel/Accept").Connect("pressed", Globals.SceneRef.Root.GetNode<Object>("/root/GodotHub"), nameof(InstallInstance), new Array { exportTemplates, monoExportTemplates, source, win64, monoWin64, x11, monoX11, headless, server, monoHeadless, monoServer, version, subVersion, releaseType });
//         Globals.SceneRef.Root.GetNodeOrNull<WindowDialog>("/root/GodotHub/NewInstanceWindowDialog").Show();
//     
//     }
//
//     public static void InstallInstance(string exportTemplates, string monoExportTemplates, string source, string win64, string monoWin64, string x11, string monoX11, string headless, string server, string monoHeadless, string monoServer, string version, string subVersion, string releaseType)
//     {
//         if (!Directory.Exists(Globals.DataFolderPath))
//         {
//             Directory.CreateDirectory(Globals.DataFolderPath);
//         }
//         if (!Directory.Exists($"{Globals.DataFolderPath}Instances"))
//         {
//             Directory.CreateDirectory($"{Globals.DataFolderPath}Instances");
//         }
//         if (!Directory.Exists($"{Globals.DataFolderPath}Instances{Path.DirectorySeparatorChar}{version}-{subVersion}-{releaseType}"))
//         {
//             Directory.CreateDirectory($"{Globals.DataFolderPath}Instances{Path.DirectorySeparatorChar}{version}-{subVersion}-{releaseType}");
//         }
//         if (!Directory.Exists($"{Globals.DataFolderPath}Instances{Path.DirectorySeparatorChar}{version}-{subVersion}-{releaseType}{Path.DirectorySeparatorChar}Downloads"))
//         {
//             Directory.CreateDirectory($"{Globals.DataFolderPath}Instances{Path.DirectorySeparatorChar}{version}-{subVersion}-{releaseType}{Path.DirectorySeparatorChar}Downloads");
//         }
//
//         var installdir = $"{Globals.DataFolderPath}Instances{Path.DirectorySeparatorChar}{version}-{subVersion}-{releaseType}{Path.DirectorySeparatorChar}";
//         var downloadDir = $"{installdir}Downloads{Path.DirectorySeparatorChar}";
//
//
//         //Download
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButtonExportTemplates").Pressed)
//         {
//             Utilities.DownloadFile(exportTemplates, $"{downloadDir}export_templates");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButton2ExportTemplatesMono").Pressed)
//         {
//             Utilities.DownloadFile(monoExportTemplates, $"{downloadDir}Mono_export_templates");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerExportsSource/CheckButtonSource").Pressed)
//         {
//             Utilities.DownloadFile(source, $"{downloadDir}Source");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerWindows/CheckButtonEditor").Pressed)
//         {
//             Utilities.DownloadFile(win64, $"{downloadDir}win64");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerWindows/CheckButtonEditorMono").Pressed)
//         {
//             Utilities.DownloadFile(monoWin64, $"{downloadDir}Mono_win64");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinux/CheckButtonEditor").Pressed)
//         {
//             Utilities.DownloadFile(x11, $"{downloadDir}x11");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinux/CheckButtonEditorMono").Pressed)
//         {
//             Utilities.DownloadFile(monoX11, $"{downloadDir}Mono_x11");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonHeadless").Pressed)
//         {
//             Utilities.DownloadFile(headless, $"{downloadDir}headless");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonServer").Pressed)
//         {
//             Utilities.DownloadFile(server, $"{downloadDir}server");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonHeadlessMono").Pressed)
//         {
//             Utilities.DownloadFile(monoHeadless, $"{downloadDir}Mono_headless");
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToDownload/HBoxContainerLinuxServer/CheckButtonServerMono").Pressed)
//         {
//             Utilities.DownloadFile(monoServer, $"{downloadDir}Mono_server");
//         }
//
//
//         //Install
//
//         File.WriteAllText($"{installdir}._sc_", @"[init_projects]
//
// list=[]
//
// [presets]
//
// export/android/shutdown_adb_on_exit=true
//
// ");
//
//         if (subVersion  == "")
//         {
//             subVersion = releaseType.ToLower();
//             //Version+"-"+SubVersion
//         }
//
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerExportsSource/CheckButtonExportTemplates").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}export_templates_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}export_templates_out");
//             }
//             Utilities.UnZip($"{downloadDir}export_templates", $"{downloadDir}export_templates_out");
//
//             if (!Directory.Exists($"{installdir}editor_data"))
//             {
//                 Directory.CreateDirectory($"{installdir}editor_data");
//             }
//
//             if (!Directory.Exists($"{installdir}editor_data{Path.DirectorySeparatorChar}templates"))
//             {
//                 Directory.CreateDirectory($"{installdir}editor_data{Path.DirectorySeparatorChar}templates");
//             }
//
//             if (!Directory.Exists($"{installdir}editor_data{Path.DirectorySeparatorChar}templates{Path.DirectorySeparatorChar}{version}.stable"))
//             {
//                 Directory.CreateDirectory($"{installdir}editor_data{Path.DirectorySeparatorChar}templates{Path.DirectorySeparatorChar}{version}.stable");
//             }
//
//             Utilities.DirectoryCopy($"{downloadDir}export_templates_out{Path.DirectorySeparatorChar}templates", $"{installdir}editor_data{Path.DirectorySeparatorChar}templates{Path.DirectorySeparatorChar}{version}.stable");
//             Directory.Delete($"{downloadDir}export_templates_out", true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerExportsSource/CheckButton2ExportTemplatesMono").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}Mono_export_templates_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}Mono_export_templates_out");
//             }
//             Utilities.UnZip($"{downloadDir}Mono_export_templates", $"{downloadDir}Mono_export_templates_out");
//
//             if (!Directory.Exists($"{installdir}editor_data"))
//             {
//                 Directory.CreateDirectory($"{installdir}editor_data");
//             }
//
//             if (!Directory.Exists($"{installdir}editor_data{Path.DirectorySeparatorChar}templates"))
//             {
//                 Directory.CreateDirectory($"{installdir}editor_data{Path.DirectorySeparatorChar}templates");
//             }
//
//             if (!Directory.Exists($"{installdir}editor_data{Path.DirectorySeparatorChar}templates{Path.DirectorySeparatorChar}{version}.stable.mono"))
//             {
//                 Directory.CreateDirectory($"{installdir}editor_data{Path.DirectorySeparatorChar}templates{Path.DirectorySeparatorChar}{version}.stable.mono");
//             }
//
//             Utilities.DirectoryCopy($"{downloadDir}Mono_export_templates_out{Path.DirectorySeparatorChar}templates", $"{installdir}editor_data{Path.DirectorySeparatorChar}templates{Path.DirectorySeparatorChar}{version}.stable.mono");
//             Directory.Delete($"{downloadDir}Mono_export_templates_out", true);
//         }
//         //if (Globals.SceneRef.Root.GetNodeOrNull<Godot.CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerExportsSource/CheckButtonSource").Pressed)
//         //{
//             //Not installable/extractable
//             //if (!System.IO.Directory.Exists(DownloadDir + "Source_out"))
//             //{
//             //    System.IO.Directory.CreateDirectory(DownloadDir + "Source_out");
//             //}
//             //Utilities.UnZip(DownloadDir + "Source", DownloadDir + "Source_out");
//         //}
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerWindows/CheckButtonEditor").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}win64_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}win64_out");
//             }
//             Utilities.UnZip($"{downloadDir}win64", $"{downloadDir}win64_out");
//             Utilities.DirectoryCopy($"{downloadDir}win64_out",  installdir);
//             Directory.Delete($"{downloadDir}win64_out", true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerWindows/CheckButtonEditorMono").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}Mono_win64_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}Mono_win64_out");
//             }
//             Utilities.UnZip($"{downloadDir}Mono_win64", $"{downloadDir}Mono_win64_out");
//             Utilities.DirectoryCopy($"{downloadDir}Mono_win64_out{Path.DirectorySeparatorChar}Godot_v{version}-{subVersion}_mono_win64",  installdir);
//             Directory.Delete($"{downloadDir}Mono_win64_out", true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinux/CheckButtonEditor").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}x11_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}x11_out");
//             }
//             Utilities.UnZip($"{downloadDir}x11", $"{downloadDir}x11_out");
//             Utilities.DirectoryCopy($"{downloadDir}x11_out",  installdir);
//             Directory.Delete($"{downloadDir}x11_out", true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinux/CheckButtonEditorMono").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}Mono_x11_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}Mono_x11_out");
//             }
//             Utilities.UnZip($"{downloadDir}Mono_x11", $"{downloadDir}Mono_x11_out");
//             Utilities.DirectoryCopy($"{downloadDir}Mono_x11_out{Path.DirectorySeparatorChar}Godot_v{version}-{subVersion}_mono_x11_64",  installdir);
//             Directory.Delete($"{downloadDir}Mono_x11_out", true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonHeadless").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}headless_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}headless_out");
//             }
//             Utilities.UnZip($"{downloadDir}headless", $"{downloadDir}headless_out");
//             Utilities.DirectoryCopy($"{downloadDir}headless_out",  installdir);
//             Directory.Delete($"{downloadDir}headless_out", true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonServer").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}server_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}server_out");
//             }
//             Utilities.UnZip($"{downloadDir}server", $"{downloadDir}server_out");
//             Utilities.DirectoryCopy($"{downloadDir}server_out",  installdir);
//             Directory.Delete($"{downloadDir}server_out",  true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonHeadlessMono").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}Mono_headless_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}Mono_headless_out");
//             }
//             Utilities.UnZip($"{downloadDir}Mono_headless", $"{downloadDir}Mono_headless_out");
//             Utilities.DirectoryCopy($"{downloadDir}Mono_headless_out{Path.DirectorySeparatorChar}Godot_v{version}-{subVersion}_mono_linux_headless_64",  installdir);
//             Directory.Delete($"{downloadDir}Mono_headless_out",  true);
//         }
//         if (Globals.SceneRef.Root.GetNodeOrNull<CheckButton>("/root/GodotHub/NewInstanceWindowDialog/MainVBoxContainer/VBoxContainerWhatToInstall/HBoxContainerLinuxServer/CheckButtonServerMono").Pressed)
//         {
//             if (!Directory.Exists($"{downloadDir}Mono_server_out"))
//             {
//                 Directory.CreateDirectory($"{downloadDir}Mono_server_out");
//             }
//             Utilities.UnZip($"{downloadDir}Mono_server", $"{downloadDir}Mono_server_out");
//             Utilities.DirectoryCopy($"{downloadDir}Mono_server_out{Path.DirectorySeparatorChar}Godot_v{version}-{subVersion}_mono_linux_server_64",  installdir);
//             Directory.Delete($"{downloadDir}Mono_server_out", true);
//         }
//         Globals.SceneRef.Root.GetNodeOrNull<WindowDialog>("/root/GodotHub/NewInstanceWindowDialog").Hide();
//     }
// }