using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GodotHub.Model;
using HtmlAgilityPack;

public static class Util
{
    private const string TUX_FAMILY_GODOT_BASE_URL = "https://downloads.tuxfamily.org/godotengine/";

    public static async IAsyncEnumerable<Release> GetGodotStable()
    {
        var linksFromUrl = await GetLinksFromUrl(TUX_FAMILY_GODOT_BASE_URL);
        foreach (var stableVersions in linksFromUrl)
        {
            var croppedStableVersion = stableVersions[..^1];
            if (stableVersions != "../" && stableVersions != "media/" && stableVersions != "patreon/" && stableVersions != "testing/" && stableVersions != "toolchains/")
            {
                var newStableRelease = new Release
                {
                    ReleaseType = ReleaseTypes.STABLE,
                    Version = croppedStableVersion
                };
                var newStableReleaseIsValid = false;

                var links = await GetLinksFromUrl(TUX_FAMILY_GODOT_BASE_URL + stableVersions);
                foreach (var stableVersionsDir in links.Where(possibleStableVersionFilesDirectory => possibleStableVersionFilesDirectory != "../"))
                {
                    if (stableVersionsDir.Contains("alpha")) //is a alpha version directory
                    {
                        GetGodotSubVersion(TUX_FAMILY_GODOT_BASE_URL + stableVersions + stableVersionsDir, croppedStableVersion, stableVersionsDir.Substring(0, stableVersionsDir.Length - 1), ReleaseTypes.ALPHA);
                    }
                    else if (stableVersionsDir.Contains("beta")) //is a beta version directory
                    {
                        GetGodotSubVersion(TUX_FAMILY_GODOT_BASE_URL + stableVersions + stableVersionsDir, croppedStableVersion, stableVersionsDir.Substring(0, stableVersionsDir.Length - 1), ReleaseTypes.BETA);
                    }
                    else if (stableVersionsDir.Contains("rc")) //is a rc version directory
                    {
                        GetGodotSubVersion(TUX_FAMILY_GODOT_BASE_URL + stableVersions + stableVersionsDir, croppedStableVersion, stableVersionsDir.Substring(0, stableVersionsDir.Length - 1), ReleaseTypes.RC);
                    }
                    else if (stableVersionsDir.Contains("mono")) //is a mono version directory
                    {
                        var croppedStableVersionUrl = $"{TUX_FAMILY_GODOT_BASE_URL}{croppedStableVersion}/mono/";
                        foreach (var possibleStableVersionFilesDirectoryMono in await GetLinksFromUrl(croppedStableVersionUrl))
                        {
                            if (possibleStableVersionFilesDirectoryMono.Equals($"Godot_v{croppedStableVersion}-stable_mono_win64.zip"))
                            {
                                newStableRelease.Mono_win64 = $"{croppedStableVersionUrl}{possibleStableVersionFilesDirectoryMono}";
                                newStableReleaseIsValid = true;
                            }
                            else if (possibleStableVersionFilesDirectoryMono.Equals($"Godot_v{croppedStableVersion}-stable_mono_x11_64.zip"))
                            {
                                newStableRelease.Mono_x11 = $"{croppedStableVersionUrl}{possibleStableVersionFilesDirectoryMono}";
                                newStableReleaseIsValid = true;
                            }
                            else if (possibleStableVersionFilesDirectoryMono.Equals($"Godot_v{croppedStableVersion}-stable_mono_export_templates.tpz"))
                            {
                                newStableRelease.Mono_export_templates = $"{croppedStableVersionUrl}{possibleStableVersionFilesDirectoryMono}";
                                newStableReleaseIsValid = true;
                            }
                            else if (possibleStableVersionFilesDirectoryMono.Equals($"Godot_v{croppedStableVersion}-stable_mono_linux_headless_64.zip"))
                            {
                                newStableRelease.Mono_headless = $"{croppedStableVersionUrl}{possibleStableVersionFilesDirectoryMono}";
                                newStableReleaseIsValid = true;
                            }
                            else if (possibleStableVersionFilesDirectoryMono.Equals($"Godot_v{croppedStableVersion}-stable_mono_linux_server_64.zip"))
                            {
                                newStableRelease.Mono_server = $"{croppedStableVersionUrl}{possibleStableVersionFilesDirectoryMono}";
                                newStableReleaseIsValid = true;
                            }
                        }
                    }
                    else //is likely just release files
                    {
                        var croppedStableVersionUrl = $"{TUX_FAMILY_GODOT_BASE_URL}{croppedStableVersion}/{stableVersionsDir}";
                        if (stableVersionsDir == $"godot-{croppedStableVersion}-stable.tar.xz")
                        {
                            newStableRelease.Source = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == "README.txt")
                        {
                            newStableRelease.Changelog = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == $"Godot_v{croppedStableVersion}-stable_changelog_chrono.txt")
                        {
                            newStableRelease.Changelog = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == $"Godot_v{croppedStableVersion}-stable_win64.exe.zip")
                        {
                            newStableRelease.win64 = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == $"Godot_v{croppedStableVersion}-stable_x11.64.zip")
                        {
                            newStableRelease.x11 = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == $"Godot_v{croppedStableVersion}-stable_export_templates.tpz")
                        {
                            newStableRelease.export_templates = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == $"Godot_v{croppedStableVersion}-stable_linux_headless.64.zip")
                        {
                            newStableRelease.headless = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                        else if (stableVersionsDir == $"Godot_v{croppedStableVersion}-stable_linux_server.64.zip")
                        {
                            newStableRelease.server = $"{croppedStableVersionUrl}";
                            newStableReleaseIsValid = true;
                        }
                    }
                }

                if (newStableReleaseIsValid)
                {
                    yield return newStableRelease;
                }
            }
        }
    }

    public static async IAsyncEnumerable<(ReleaseTypes type, Release subRelease)> GetGodotSubVersion(string url, string stableVersion, string subversion, ReleaseTypes type)
    {
        var subRelease = new Release
        {
            ReleaseType = type,
            Version = stableVersion,
            SubVersion = subversion
        };
        var newSubReleaseIsValid = false;


        var fromUrl = await GetLinksFromUrl(url);
        foreach (var possibleSubReleaseFilesDirectory in fromUrl.Where(possibleSubReleaseFilesDirectory => possibleSubReleaseFilesDirectory != "../"))
        {
            if (possibleSubReleaseFilesDirectory.Length >= 4 && possibleSubReleaseFilesDirectory.Substring(0, 4) == "mono")
            {
                foreach (var possibleSubReleaseFilesDirectoryMono in await GetLinksFromUrl($"{url}mono"))
                {
                    if (possibleSubReleaseFilesDirectoryMono == $"Godot_v{stableVersion}-{subversion}_mono_win64.zip")
                    {
                        subRelease.Mono_win64 = $"{url}/mono/{possibleSubReleaseFilesDirectoryMono}";
                        newSubReleaseIsValid = true;
                    }
                    else if (possibleSubReleaseFilesDirectoryMono == $"Godot_v{stableVersion}-{subversion}_mono_x11_64.zip")
                    {
                        subRelease.Mono_x11 = $"{url}/mono/{possibleSubReleaseFilesDirectoryMono}";
                        newSubReleaseIsValid = true;
                    }
                    else if (possibleSubReleaseFilesDirectoryMono == $"Godot_v{stableVersion}-{subversion}_mono_linux_64.zip")
                    {
                        subRelease.Mono_x11 = $"{url}/mono/{possibleSubReleaseFilesDirectoryMono}";
                        newSubReleaseIsValid = true;
                    }
                    else if (possibleSubReleaseFilesDirectoryMono == $"Godot_v{stableVersion}-{subversion}_mono_export_templates.tpz")
                    {
                        subRelease.Mono_export_templates = $"{url}/mono/{possibleSubReleaseFilesDirectoryMono}";
                        newSubReleaseIsValid = true;
                    }
                    else if (possibleSubReleaseFilesDirectoryMono == $"Godot_v{stableVersion}-{subversion}_mono_linux_headless_64.zip")
                    {
                        subRelease.Mono_headless = $"{url}/mono/{possibleSubReleaseFilesDirectoryMono}";
                        newSubReleaseIsValid = true;
                    }
                    else if (possibleSubReleaseFilesDirectoryMono == $"Godot_v{stableVersion}-{subversion}_mono_linux_server_64.zip")
                    {
                        subRelease.Mono_server = $"{url}/mono/{possibleSubReleaseFilesDirectoryMono}";
                        newSubReleaseIsValid = true;
                    }
                }
            }
            //is likely just release files
            else
            {
                if (possibleSubReleaseFilesDirectory == $"godot-{stableVersion}-{subversion}.tar.xz")
                {
                    subRelease.Source = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == "README.txt")
                {
                    subRelease.Changelog = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_changelog_chrono.txt")
                {
                    subRelease.Changelog = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_win64.exe.zip")
                {
                    subRelease.win64 = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_x11.64.zip")
                {
                    subRelease.x11 = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_linux.64.zip")
                {
                    subRelease.x11 = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_export_templates.tpz")
                {
                    subRelease.export_templates = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_linux_headless.64.zip")
                {
                    subRelease.headless = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
                else if (possibleSubReleaseFilesDirectory == $"Godot_v{stableVersion}-{subversion}_linux_server.64.zip")
                {
                    subRelease.server = url + possibleSubReleaseFilesDirectory;
                    newSubReleaseIsValid = true;
                }
            }
        }

        if (newSubReleaseIsValid)
            yield return (type, subRelease);
    }

    public static async Task<IEnumerable<string>> GetLinksFromUrl(string url)
    {
        ServicePointManager.ServerCertificateValidationCallback += (sender, cer, chain, sslPolicyErrors) => true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        var document = new HtmlDocument();
        using (var client = new HttpClient())
        {
            var responseMessage = await client.GetAsync(url);
            document.Load(await responseMessage.Content.ReadAsStreamAsync());
        }

        var anchors = document.DocumentNode.SelectNodes("//a");
        return anchors.Select(anchor => anchor.Attributes["href"].Value);
    }

    public static async Task GetGodotVersions()
    {
        var linksFromUrl = await GetLinksFromUrl(TUX_FAMILY_GODOT_BASE_URL);
        foreach (var item in linksFromUrl)
        {
            await GetLinksFromUrl(TUX_FAMILY_GODOT_BASE_URL + item);
        }
    }

    public static void UnZip(string origin, string destination)
    {
        ZipFile.ExtractToDirectory(origin, destination, Encoding.UTF8);
    }

    public static void DownloadFile(string url, string destination)
    {
        var client = new WebClient();
        client.DownloadFile(new Uri(url), destination);
    }

    public static bool CheckIfUrlIsAccessible(string url)
    {
        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Timeout = 1000;
        request.Method = "HEAD";
        try
        {
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
        catch (WebException)
        {
            return false;
        }
    }

    public static string GetFileContentLength(string url)
    {
        //https://github.com/chaosvolt/cdda-tankmod-revived-mod/archive/master.zip
        var size = "0";
        var tries = 5;


        //Check if url is accessible
        if (CheckIfUrlIsAccessible(url))
        {
            while (size == "0" && tries > 0)
            {
                var webClient = new WebClient();
                try
                {
                    webClient.OpenRead(url);
                    webClient.Headers[HttpRequestHeader.AcceptEncoding] = "*";
                    var totalSizeBytes = Convert.ToInt64(webClient.ResponseHeaders["Content-Length"]);
                    size = $"{(totalSizeBytes)}";
                    tries--;
                }
                catch (Exception)
                {
                    Thread.Sleep(100);
                    tries--;
                }
            }

            return size;
        }

        return "0";
    }

    internal static void MoveFolder(string origin, string destination)
    {
        var dirInfo = new DirectoryInfo(destination);
        var filesToMove = new List<string>();
        if (!string.IsNullOrEmpty(origin))
        {
            filesToMove = Directory.GetFiles(origin).ToList();
        }

        if (origin != null)
        {
            var dirsToMove = Directory.GetDirectories(origin).ToList();

            foreach (var mDir in dirsToMove.Select(dir => new DirectoryInfo(dir)))
            {
                if (!Directory.Exists(dirInfo + Path.DirectorySeparatorChar.ToString()))
                {
                    Directory.CreateDirectory(dirInfo + Path.DirectorySeparatorChar.ToString());
                }

                mDir.MoveTo(dirInfo + Path.DirectorySeparatorChar.ToString() + mDir.Name);
            }
        }

        foreach (var mFile in filesToMove.Select(file => new FileInfo(file)))
        {
            if (!Directory.Exists(Path.GetDirectoryName(dirInfo + Path.DirectorySeparatorChar.ToString() + mFile.Name)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(dirInfo + Path.DirectorySeparatorChar.ToString() + mFile.Name) ?? throw new InvalidOperationException());
            }

            mFile.MoveTo(dirInfo + Path.DirectorySeparatorChar.ToString() + mFile.Name);
        }
    }

    public static string GetStringFromWebRequest(string url)
    {
        if (WebRequest.Create(url) is HttpWebRequest webRequest)
        {
            webRequest.Method = "GET";
            webRequest.UserAgent = "Anything";
            webRequest.ServicePoint.Expect100Continue = false;
            try
            {
                using (var responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    var reader = responseReader.ReadToEnd();
                    return reader;
                }
            }
            catch
            {
                return "";
            }
        }

        return "";
    }

    public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true)
    {
        // Get the subdirectories for the specified directory.
        var dir = new DirectoryInfo(sourceDirName);

        if (!dir.Exists)
        {
            //throw new DirectoryNotFoundException(
            //    "Source directory does not exist or could not be found: "
            //    + sourceDirName);
            // Godot.GD.PrintErr("Does Not exist:", sourceDirName);
            return;
        }
        else
        {
            var dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (var subDir in dirs)
                {
                    var tempPath = Path.Combine(destDirName, subDir.Name);
                    DirectoryCopy(subDir.FullName, tempPath, copySubDirs);
                }
            }
        }
    }
}