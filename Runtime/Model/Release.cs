namespace GodotHub.Model;

public record Release
{
    public ReleaseTypes ReleaseType;
    public string Version = ""; //Stable Version
    public string SubVersion = ""; //Beta,rc,alpha Version

    public string Source = "";
    public string Changelog = "";
    public string win64 = "";
    public string x11 = "";
    public string export_templates = "";
    public string headless = "";
    public string server = "";

    public string Mono_win64 = "";
    public string Mono_x11 = "";
    public string Mono_export_templates = "";
    public string Mono_headless = "";
    public string Mono_server = "";
}