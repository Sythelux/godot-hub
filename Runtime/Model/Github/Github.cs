using System;
using System.Collections.Generic;

namespace GodotHub.Model.Github
{
    [Serializable]
    public record struct RateLimit
    {
        public RateLimitRate rate;
    }

    [Serializable]
    public record struct RateLimitRate
    {
        public string limit;
        public string remaining;
        public string reset;
        public string used;
    }

    [Serializable]
    public record struct ReleasesTags
    {
        public string @ref;
        public string node_id;
        public string url;
        public ReleasesAssetsTags @object;
    }

    [Serializable]
    public record struct ReleasesAssetsTags
    {
        public string sha;
        public string type;
        public string url;
    }

    [Serializable]
    public struct Release
    {
        public string url;
        public string assets_url;
        public string upload_url;
        public string html_url;
        public string id;
        public string node_id;
        public string tag_name;
        public string target_commitish;
        public string name;
        public string draft;
        public string created_at;
        public string published_at;
        public List<ReleaseAssets> assets;
        public string body;
        public string message;
    }

    [Serializable]
    public struct ReleaseAssets
    {
        public string url;
        public string id;
        public string node_id;
        public string name;
        public string label;
        public string content_type;
        public string state;
        public string size;
        public string download_count;
        public string created_at;
        public string updated_at;
        public string browser_download_url;
    }
}