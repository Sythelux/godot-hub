namespace GodotHub.Model;

public enum ReleaseTypes
{
    STABLE,
    BETA,
    RC,
    ALPHA
}