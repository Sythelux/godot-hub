using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;
using CommunityToolkit.Mvvm.ComponentModel;
using Godot;

namespace GodotHub.ViewModels;

[INotifyPropertyChanged]
public partial class NewsViewModel
{
    // private static readonly ILog Log = LogManager.GetLogger(typeof(EditorsViewModel));

    [ObservableProperty]
    protected ObservableCollection<NewsElementViewModel> news = new();

    public NewsViewModel()
    {
        Task.Run(LoadNews);
    }

    private Task LoadNews()
    {
        SyndicationFeed feed = null;

        try
        {
            using var reader = XmlReader.Create("https://godotengine.org/rss.xml");
            feed = SyndicationFeed.Load(reader);
        }
        catch (Exception e)
        {
            GD.PrintErr(e);
        }

        if (feed != null)
        {
            foreach (var element in feed.Items.Select(item => new NewsElementViewModel
                     {
                         Title = item.Title.Text,
                         Summary = item.Summary.Text,
                         Date = item.PublishDate.Date.ToShortDateString(),
                         Url = item.Id
                     }))
            {
                news.Add(element);
            }
        }

        return Task.CompletedTask;
    }
}