using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Godot;
using GodotHub.Model;

namespace GodotHub.ViewModels;

[INotifyPropertyChanged]
public partial class EditorElementViewModel
{
    [ObservableProperty]
    protected Texture icon;

    [ObservableProperty]
    protected ReleaseTypes releaseType;

    [ObservableProperty]
    [NotifyPropertyChangedFor(nameof(FullVersion))]
    protected string version = ""; //Stable Version

    [ObservableProperty]
    [NotifyPropertyChangedFor(nameof(FullVersion))]
    protected string subVersion = ""; //RC1, Beta5, alpha3

    [ObservableProperty]
    protected string installPath;

    [ObservableProperty]
    protected bool hasExportTemplates;

    [ObservableProperty]
    protected bool hasMonoSupport;

    public string FullVersion => $"{Version} - {(string.IsNullOrEmpty(SubVersion) ? "Release" : SubVersion)}";

    // [ObservableProperty]
    // private string source = "";
    //
    // [ObservableProperty]
    // private string changelog = "";
    //
    // [ObservableProperty]
    // private string win64 = "";
    //
    // [ObservableProperty]
    // private string x11 = "";
    //
    // [ObservableProperty]
    // private string exportTemplates = "";
    //
    // [ObservableProperty]
    // private string headless = "";
    //
    // [ObservableProperty]
    // private string server = "";
    //
    // [ObservableProperty]
    // private string monoWin64 = "";
    //
    // [ObservableProperty]
    // private string monoX11 = "";
    //
    // [ObservableProperty]
    // private string monoExportTemplates = "";
    //
    // [ObservableProperty]
    // private string monoHeadless = "";
    //
    // [ObservableProperty]
    // private string monoServer = "";

    [RelayCommand]
    public void EditEditor()
    {
    }
}