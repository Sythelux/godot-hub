namespace GodotHub.ViewModels;

public enum ViewEnum
{
    Editors,
    News,
    Projects,
    Learn,
    Community
}