using CommunityToolkit.Mvvm.ComponentModel;
using Godot;

namespace GodotHub.ViewModels;

[INotifyPropertyChanged]
public partial class MainWindowViewModel
{
    [ObservableProperty]
    protected Texture logo;

    [ObservableProperty]
    protected EditorsViewModel editorsView = new();

    [ObservableProperty]
    protected ProjectsViewModel projectsView = new();

    [ObservableProperty]
    protected NewsViewModel newsView = new();

    [ObservableProperty]
    protected ViewEnum selectedView = ViewEnum.Projects;

    public MainWindowViewModel()
    {
        // Logo = new Bitmap(Assets.icon);
    }
}