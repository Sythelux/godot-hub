using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace GodotHub.ViewModels;

[INotifyPropertyChanged]
public partial class NewsElementViewModel
{
    public static readonly Regex HTML_TAGS = new("<.*?>");

    [ObservableProperty]
    public string title;

    [ObservableProperty]
    [NotifyPropertyChangedFor(nameof(ShortSummary))]
    public string summary;

    public string ShortSummary => HTML_TAGS.Replace(summary, "")[..100];

    [ObservableProperty]
    public string url;

    [ObservableProperty]
    public string date;

    [RelayCommand]
    public void ReadAll()
    {
        try
        {
            Process.Start(url);
        }
        catch
        {
            // hack because of this: https://github.com/dotnet/corefx/issues/10361
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                Process.Start(new ProcessStartInfo(url.Replace("&", "^&")) { UseShellExecute = true });
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                Process.Start("xdg-open", url);
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                Process.Start("open", url);
            else
                throw;
        }
    }
}