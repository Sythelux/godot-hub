using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using Godot;
using GodotHub.Model;
// using log4net;
using File = System.IO.File;

namespace GodotHub.ViewModels;

[INotifyPropertyChanged]
public partial class EditorsViewModel
{
    // private static readonly ILog Log = LogManager.GetLogger(typeof(EditorsViewModel));
    private static readonly Regex Numbers = new("\\d");

    [ObservableProperty]
    protected ObservableCollection<EditorElementViewModel> editors = new();

    public EditorsViewModel()
    {
        // Task.Run(LoadEditorsOnline);
        Task.Run(LoadEditorsInstalled);
    }

    protected virtual Task LoadEditorsInstalled()
    {
        try
        {
            var directoryInfo = new DirectoryInfo("/opt/Godot/"); //TODO: make configurable
            foreach (var directory in directoryInfo.EnumerateDirectories())
            {
                if (directory.EnumerateFiles().Any(f => f.Name == "_sc_"))
                {
                    var executable = directory.EnumerateFiles().FirstOrDefault(f => f.Name.Contains("Godot"))?.Name.Split("_");
                    var versions = executable?[1].Split("-");
                    var editorElementViewModel = new EditorElementViewModel
                    {
                        InstallPath = directory.FullName,
                        Version = versions?[0],
                        ReleaseType = Enum.Parse<ReleaseTypes>(Numbers.Replace(versions?[1] ?? "None", "").ToUpper()),
                        SubVersion = versions?[1],
                        HasMonoSupport = File.Exists(Path.Join(directory.FullName, "GodotSharp")),
                        HasExportTemplates = File.Exists(Path.Join(directory.FullName, $"editor_data{Path.PathSeparator}templates"))
                    };
                    editors.Add(editorElementViewModel);
                }
            }
        }
        catch (Exception e)
        {
            GD.PrintErr(e);
        }

        return Task.CompletedTask;
    }

    protected virtual async Task LoadEditorsOnline()
    {
        try
        {
            await foreach (var release in Util.GetGodotStable())
            {
                editors.Add(new EditorElementViewModel
                {
                    Version = release.Version,
                    SubVersion = release.SubVersion,
                    InstallPath = release.Source
                });
            }
        }
        catch (Exception e)
        {
            GD.PrintErr(e);
        }
    }
}